
////////////////------------------Order Class-------------////////////////////////////
class Order {
    constructor(name, price) {
        this.name = name;
        //this.price=`$${price}`;// it is better not to format the number here for future math calculation we can put the dolar sign at the printing function
        this.price = price;
    }

}

////////////////------------------Orders Class-------------////////////////////////////
class Orders {

    constructor() {
        this.items = [];
    }

    /**
     * Setter method
     * @param  order 
     */
    setOrder(order) {
        this.items.push(order);
    }

    /**
     * Getter method
     */
    getList() {
        return this.items;
    }

    /**
     * Total method
     * @param  total 
     */
    getTotal() {
        let total = 0;
        // Arrow function in EcmaScript6
        this.items.forEach((item) => {
            total = total + item.price;
        });
        return total;
    }

    /**
     * avarage method
     * @param  avg,total 
     */
    getAvg() {
        let avg = 0;
        let total = 0;
        this.items.forEach((item) => {
            total = total + item.price;
            avg = total / this.items.length;
        });
        return avg;
    }

    /**
     * Sort Asc method
     * @param  sortBy 
     */
    sortAsc(sortBy) {
        if (sortBy == "name")
            this.items.sort((a, b) => (a.name > b.name) ? 1 : -1);
        else
            this.items.sort((a, b) => (a.price > b.price) ? 1 : -1);
    }

    /**
     * Sort Desc method
     * @param  sortBy 
     */
    sortDesc(sortBy) {
        if (sortBy == "name")
            this.items.reverse((a, b) => (a.name > b.name) ? 1 : -1);
        else
            this.items.reverse((a, b) => (a.price > b.price) ? 1 : -1);

    }

    /**
     * Delete method
     * @param  index 
     */
    deleteOrder(index) {

        this.items.splice(index, 1);

    }
}
////////////////------------------ShoppingCart Class-------------////////////////////////////
class ShoppingCart {

    constructor() {
        this.orderTable = document.querySelector('.table');
        this.initialize();
    }

    initialize() {
        this.orderTable.innerHTML = `
                            <thead>
                                <tr style=" background-color:gray;color:white">
                                    <th scope="col" >#</th>
                                    <th scope="col"><i class="fa fa-sort mr-4 fa-lg" onclick="sort('name')"></i>Order Name</th>
                                    <th scope="col"><i class="fa fa-sort mr-4 fa-lg" onclick="sort('price')"></i>Order Price</th>
                                  <th></th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr><td colspan="4" class="text-center">Enter Your Orders</td></tr>
                            </tbody>
                            <tfoot>
                            
                                <tr style=" background-color:gray;color:white">
                                   <td class="order-count"></td>
                                    <td>Total Price</td>
                                    <td class="order-total"></td>
                                    <td></td>
                                </tr>
                                  <tr style=" background-color:gray;color:white">
                                    <td></td>
                                    <td  >Total Avarage</td>
                                    <td class="order-avarage"></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                            `;
    }

    renderRows(rowList) {
        let tableRow = '';
        for (let orderIndex = 0; orderIndex < rowList.length; orderIndex++) {
            const order = rowList[orderIndex];
            const orderName = order.name;
            const orderPrice = order.price;
            tableRow = tableRow + `<tr>
                                    <th scope="row">${orderIndex + 1}</th>
                                    <td>${orderName}</td>
                                    <td>$${orderPrice}</td>
                                     <td> <i class="fa fa-trash" aria-hidden="true" onclick='deleOrder(${orderIndex})'></i></td>
                                </tr>`;
        }

        const tableBodyElement = document.querySelector('table > tbody');
        tableBodyElement.innerHTML = tableRow;
        let avarage = `$${orders.getAvg()}`;
        let total = `$${orders.getTotal()}`;
        document.querySelector('.order-count').innerHTML = rowList.length;
        document.querySelector('.order-total').innerHTML = total;
        document.querySelector('.order-avarage').innerHTML = avarage;
    }
}


/*--------------------------------End of Classes---------------------------*/







const orders = new Orders();
const tableBodyElement = document.querySelector('.table');
const shoppingCart = new ShoppingCart();
shoppingCart.initialize();




//HandleOrder Form
function handleOrderForm(event) {
    event.preventDefault();
    let orderPrice;
    const orderNameElement = document.querySelector('#orderName');
    const orderPriceElement = document.querySelector('#orderPrice');
   if(orderPriceElement.value=="")
   orderPrice=0;
       
     if((orderNameElement.value!="") && ( orderPrice!=0)){
    const newOrder = new Order(orderNameElement.value, parseInt(orderPriceElement.value));
    orders.setOrder(newOrder);
    shoppingCart.renderRows(orders.getList());
     }   
else 
    
    alert("Please Check Your Entries and try again!");
    orderPriceElement.value = '';
    orderNameElement.value = '';
}



/**
 * Delete order  
 * @param  index 
 */

function deleOrder(index) {
    let confDele=confirm("Are you sure you want to delete this Item")
    if (confDele == true) {
        orders.deleteOrder(index);
        shoppingCart.renderRows(orders.getList());
      } 
   
}


     /**
     * sort order  
     * @param sortBy,clik
     */

function sort(sortBy) {
    let clik = click();
    if (clik == 0)
        orders.sortAsc(sortBy);
    else
        orders.sortDesc(sortBy);
    shoppingCart.initialize();
    shoppingCart.renderRows(orders.getList());
}

      /**
     * sort order  
     * 
     */
let clicks = 0;
function click() {
    if (clicks == 0)
        clicks = 1;
    else
        clicks = 0;
    return clicks;
}

